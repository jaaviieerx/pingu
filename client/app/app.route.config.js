(function () {
  angular
    .module("PAF")
    .config(UIRouterAppConfig);
  UIRouterAppConfig.$inject = ["$stateProvider", "$urlRouterProvider"];

  function UIRouterAppConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state("login", {
        url: "/login",
        views: {
          'menu': {
            templateUrl: 'app/menu/menu.html',
            controller: 'MenuCtrl as ctrl',
          },
          'content': {
            templateUrl: 'app/login/login.html',
            controller: 'LoginCtrl as ctrl',
          }
        },
        resolve: {
          user: function (PassportSvc) {
            return PassportSvc.userAuth()
              .then(function (result) {
                return result.data.user;
              })
              .catch(function (err) {
                return '';
              });
          }
        },
      })
      .state('home', {
        url: '/home',
        views: {
          'menu': {
            templateUrl: 'app/menu/menu.html',
            controller: 'MenuCtrl as ctrl',
          },
          'content': {
            templateUrl: 'app/home/home.html',
            controller: 'HomeCtrl as ctrl',
          }
        },
        resolve: {
          user: function (PassportSvc) {
            return PassportSvc.userAuth()
              .then(function (result) {
                return result.data.user;
              })
              .catch(function (err) {
                return '';
              });
          }
        },
      })
      .state('reg', {
        url: "/reg",
        views: {
          'menu': {
            templateUrl: 'app/menu/menu.html',
            controller: 'MenuCtrl as ctrl',
          },
          'content': {
            templateUrl: 'app/register/register.html',
            controller: 'RegCtrl as ctrl',
          }
        },
        resolve: {
          user: function (PassportSvc) {
            return PassportSvc.userAuth()
              .then(function (result) {
                return result.data.user;
              })
              .catch(function (err) {
                return '';
              });
          }
        },
      });
    $urlRouterProvider.otherwise("/login");
  }
})();
