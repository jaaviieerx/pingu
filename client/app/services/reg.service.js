(function(){
    angular
        .module("RegApp")
        .service("RegServiceAPI",[
            '$http',
            RegService
        ])
})();

function RegService($http){
    var ctrl = this;
    ctrl.register = register;

        function register(user){
                console.log(user);
                return $http.post("/register", user);
        }
};