(function() {
    angular
      .module('PAF')
      .service('PassportSvc', PassportSvc);
  
    PassportSvc.$inject = [ "$http", "$state" ];
  
    function PassportSvc($http, $state) {
      var ctrl = this;
  
      ctrl.login = login;
      ctrl.userAuth = userAuth;
  
      function userAuth() {
        return $http.get(
          '/user/auth');
      }
  
      function login(user) {
        /*return $http.post(
          '/login',
          user
        );*/
        $state.go('home');
      }
    }
  })();