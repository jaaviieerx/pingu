(function() {
    angular
      .module('PAF')
      .controller('LoginCtrl', LoginCtrl);
  
    LoginCtrl.$inject = [ 'PassportSvc', '$state' ];
  
    function LoginCtrl(PassportSvc, $state) {
      var ctrl = this;
  
      ctrl.user = {
        username: '',
        password: '',
      };

      ctrl.msg = '';
  
      ctrl.login = login;
  
      function login() {
        console.log("logging in...");
        PassportSvc.login(ctrl.user)
          .then(function(result) {
            $state.go('home');
            return true;
          })
          .catch(function(err) {
            ctrl.msg = 'Invalid Username or Password!';
            ctrl.user.username = ctrl.user.password = '';
            return false;
          });
      };
    };

  })();