(function(){
    angular
        .module("RegApp")
        .controller("RegCtrl", RegCtrl);
    
    RegCtrl.$inject = ["RegServiceAPI", "$http", "$log"];

    function RegCtrl(RegServiceAPI, $log) {
        var ctrl  = this;
        
        ctrl.onReg = onReg;
        ctrl.initForm = initForm;
        ctrl.onReset = onReset; 

        ctrl.emailFormat = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        ctrl.mobileFormat = /(8|9)\d{7}/;
        ctrl.users = {};

/*
        ctrl.nationality = [
            { name: "Please select" , value:0},
            { name: "Singaporean", value: 1},
            { name: "Indonesian", value: 2},
            { name: "Thai", value: 3},
            { name: "Malaysian", value: 4},
            { name: "Australian", value: 5}      
    ];
*/

        function onReg(){
            console.log(RegServiceAPI); 
            RegServiceAPI.register(RegServiceAPI, ["RegServiceApi", "$log"])
                .then((result)=>{
                    ctrl.user = result.data;
                }).catch((error)=>{
                    console.log(error);
                })
        };
        
        function initForm(){

        };

        function onReset(){
            ctrl.user = Object.assign({}, ctrl.user);
            ctrl.registrationform.$setPristine();
            ctrl.registrationform.$setUntouched();
        };

        ctrl.initForm();
    };
    
})();    