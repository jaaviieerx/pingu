(function() {
    angular
      .module('PAF')
      .controller('MenuCtrl', MenuCtrl);
  
    MenuCtrl.$inject = [ 'user' ];
  
    function MenuCtrl(user) {
      var ctrl = this;
  
      ctrl.user = user;
    }
  })();
  