'use strict';
module.exports = function(auth, app, passport) {
    app.post("/login", passport.authenticate("local", {
        successRedirect: "/login",
        failureRedirect: "/",
        failureFlash : true   
    }));

    /*
    app.post('/register', function(req, res) {
      res.status(200).json({});
    });
    
    app.get('/user/auth', auth.isAuthenticated, function(req, res) {
      if (req.user) {
        res.status(200).json({ user: req.user });
      }
      else {
        res.sendStatus(401);
      }
    });
  
    app.get('/logout', function(req, res) {
      req.logout();
      res.redirect('/');
    })*/;
  };