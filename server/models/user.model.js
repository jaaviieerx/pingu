var Sequelize = require("sequelize");

module.exports = function(connection, Sequelize){
    var Users = connection.define('users', {
        userid: {
            type: Sequelize.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true,
            UNIQUE: true
        },
        username: {
            type: Sequelize.STRING,
            allowNull: false,
            UNIQUE: true
        },
        password: {
            type: Sequelize.STRING,
            allowNull: false
        },
        first_name:{
            type: Sequelize.STRING,
            allowNull: false
        },
        last_name:{
            type: Sequelize.STRING,
            allowNull: false
        },
        birthday:{
            type: Sequelize.DATE,
            allowNull: false
        },
        gender:{
            type: Sequelize.ENUM('M', 'F'),
            allowNull: false
        },
        mobile: {
            type: Sequelize.INTEGER,
            allowNull: false,
            UNIQUE: true
        },
        email: {
            type: Sequelize.STRING,
            allowNull: false,
            UNIQUE: true
        },
        city: {
            type: Sequelize.STRING,
            default: "Singapore" 
        },
        company: {
            type: Sequelize.STRING,
            allowNull: false,
            UNIQUE: true
        },
        coid: {
            type: Sequelize.INTEGER(11),
            allowNull: false,
            AUTO_INCREMENT: true,
            UNIQUE: true
        },
        bio: {
            type: Sequelize.STRING
        },
        dispic: {
            type: Sequelize.STRING,
            default: "assets/dispic-default.png"
        },
        job: {
            type: Sequelize.STRING
        }
    }, {
        timestamps: false
    });
    return Users;
};


/*
CREATE TABLE `users` (
  `userid` int(11) NOT NULL,
  `user` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `first_name` varchar(20) NOT NULL,
  `last_name` varchar(20) NOT NULL,
  `birthday` date DEFAULT NULL,
  `gender` char(1) DEFAULT NULL,
  `mobile` int(8) NOT NULL,
  `email` varchar(45) NOT NULL,
  `city` varchar(45) NOT NULL DEFAULT 'Singapore',
  `company` varchar(255) NOT NULL,
  `coid` int(11) NOT NULL AUTO_INCREMENT,
  `bio` varchar(255) DEFAULT NULL,
  `dispic` varchar(255) DEFAULT 'assets/dispic-default.png',
  `job` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`userid`),
  UNIQUE KEY `userid_UNIQUE` (`userid`),
  UNIQUE KEY `user_UNIQUE` (`user`),
  UNIQUE KEY `coid_UNIQUE` (`coid`),
  UNIQUE KEY `mobile_UNIQUE` (`mobile`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  UNIQUE KEY `company_UNIQUE` (`company`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8
*/
/*
module.exports = {

    // Following http://docs.sequelizejs.com/en/latest/docs/models-definition/ 
    tableName: "users",

    attributes: {
        userid: {
            type: "integer",
            allowNull: "false",
            primaryKey: "true",
            autoIncrement: "true",
            UNIQUE: "true"
        },
        username: {
            type: "string",
            allowNull: "false",
            UNIQUE: "true"
        },
        password: {
            type: "string",
            allowNull: "false"
        },
        first_name: {
            type: "string",
            allowNull: "false"
        },
        last_name: {
            type: "string",
            allowNull: "false"
        },
        birthday: {
            type: "date",
            allowNull: "false"
        },
        gender: {
            type: "enum('m', 'f')",
            allowNull: "false"
        },
        mobile: {
            type: "integer",
            allowNull: "false",
            UNIQUE: "true"
        },
        email: {
            type: "string",
            allowNull: "false",
            UNIQUE: "true"
        },
        city: {
            type: "string",
            default: "Singapore"
        },
        company: {
            type: "string",
            allowNull: "false",
            UNIQUE: "true"
        },
        coid: {
            type: "integer",
            allowNull: "false",
            AUTO_INCREMENT: "true",
            UNIQUE: "true"
        },
        bio: {
            type: "string"
        },
        dispic: {
            type: "string",
            default: "assets/dispic-default.png"
        },
        job: {
            type: "string"
        }
    },


    // Associations -> http://docs.sequelizejs.com/en/latest/docs/scopes/#associations 
    associations: [{
        type: "hasMany",
        target: "username",
        options: {
            foreignkey: "company"
        }
    }],

    validate: {},
    indexes: []
};
*/

// mongodb!!!
/* db.js
module.exports = function (app) {
    var assert = require('assert');
    // Read configurations
    var getapp = require('./app');
    // Load MongoDB Nodejs Driver - MongoClient
    var MongoClient = require('mongodb').MongoClient;
    // MongoDB Connection URL
    var uri = getapp.DATABASE_URI;
    // console.log("url: " + url);
    // Create the database connection
    MongoClient.connect(uri, {
        poolSize: 10
        // other options can go here
    }, function (err, db) {
        assert.equal(null, err);
        app.locals.db = db; // access using req.app.local.db at the routes
    });
};
*/

/*
// development.js
module.exports = {
    PORT: 3000,
    DATABASE_URI: process.env.NODE_DB_URI || process.env.CLEARDB_DATABASE_URL || process.env.MONGODB_URI || 'mongodb://localhost:27017/michelin',
    // COLL_MAIN: "products",
    COLL_MAIN: "javpingu",
    COLL_USER: "javpingu",
    LIMIT: 50,

    // MongoDB Variables
    MONGO_URI: 'mongodb://javpingu:dbpassword1234@ds111476.mlab.com:11476/pingu',
    // MONGO_URI: 'mongodb://admin01:ims123@ds119370.mlab.com:19370/heroku_2rcb9ngt',
    // MONGO_URI: 'mongodb://localhost:27017/michelin',
    MONGO_DB: 'pingu',
    MONGO_HOSTNAME: 'ds111476.mlab.com',
    MONGO_PORT: 11476,
    MONGO_USERNAME: 'javpingu',
    MONGO_PASSWORD: 'password1234'
    // MYSQL variables
    // domain_name: "http://localhost:3000",
    // PORT: 3000,
    // MYSQL_DB: 'Users_sample',
    // MYSQL_USERNAME: 'root',
    // MYSQL_PASSWORD: 'ims123!@#',
    // MYSQL_HOSTNAME: 'localhost',
    // MYSQL_PORT: 3306,
    // MYSQL_LOGGING: console.log,
    // version: '1.0.0'
};
*/
/* index.js

var ENV = process.env.NODE_ENV || 'development';

module.exports = require('./' + ENV + '.js') || {};

*/

/* production.js

module.exports = {
    // MongoDB
    // MONGO_URI: 'mongodb://javpingu:password1234@ds111476.mlab.com:11476/pingu',
    // MONGO_HOSTNAME: 'ds119370.mlab.com',
    // MONGO_PORT: 19370,
    // MONGO_DB: 'heroku_2rcb9ngt',
    // MONGO_USERNAME: 'javpingu',
    // MONGO_PASSWORD: 'password1234'
    // MYSQL variables
    // domain_name: "http://localhost:4000",
    // PORT: 3000,
    // MYSQL_DB: 'Users_sample',
    // MYSQL_USERNAME: 'root',
    // MYSQL_PASSWORD: 'ims123!@#',
    // MYSQL_HOSTNAME: 'localhost',
    // MYSQL_PORT: 3306,
    // MYSQL_LOGGING: console.log,
    // version: '1.0.0'
};

*/