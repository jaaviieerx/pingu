var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;

var User = require("./models/user.model").Users;
var config = require("./config");

module.exports = function (app, passport) {

passport.use(new LocalStrategy(
  function(username, password, done) {
    User.findOne({ username: username }, function(err, user) {
      if (err) { return done(err); }
      if (!user) {
        return done(null, false, { message: 'Incorrect username.' });
      }
      if (!user.validPassword(password)) {
        return done(null, false, { message: 'Incorrect password.' });
      }
      return done(null, user);
    });
  }
));
};

/*
  function authenticateUser(username, password, done) {
    if (!password) {
      return done(null, false);
    }
    User.then(function (result) {
      if (!result) {
        return done(null, false);
      } else {
        if (bcrypt.compareSync(password, result.password)) {
          var whereClause = {};
          //User.update({ reset_password_token: null }, { where: whereClause });
          return done(null, result);
        } else {
          return done(null, false);
        }
        return done(null, result);
      }
    }).catch(function (err) {
      return done(err, false);
    });
  }

  var localAuthenticate = function (username, password, done) {
    //console.log("In authenticate function(): username=%s, password=%s", username, password);

    var valid = authenticateUser(username, password);
    if (valid) return done(null, username);
    return done(null, false);
  };

  passport.serializeUser(function (username, done) {
    console.log('passport.serializeUser: ' + username);
    done(null, username);
  });

  passport.deserializeUser(function (username, done) {
    console.log('passport.deserializeUser: ' + username);
    done(null, username);
  });

  var isAuthenticated = function (req, res, next, username, done) {
    console.log("isAuthenticated(): ", req.user);
    if (req.isAuthenticated()) {
      next();
    }
    else {
      res.sendStatus(401);
    }
  } 

return {
    isAuthenticated: isAuthenticated,
  }
};
*/