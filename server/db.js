/*var Sequelize = require('sequelize');
var config = require("./config");

console.log(config.mysql);
var database = new Sequelize(config.mysql, {
    pool: {
        max: 2,
        min: 1,
        idle: 10000
    }
});

var User = require("./models/user.model.js")(database);
//var Post = require("./models/post.model.js")(database);
//var Comment = require("./models/comment.model.js")(database);
//var AuthProvider = require("./models/authentication.provider.model.js")(database);

// BEGIN: MYSQL RELATIONS

User.hasMany(Post);
Post.belongsTo(User);
Post.hasMany(Comment);
Comment.belongsTo(Post);
Comment.belongsTo(User);
User.hasMany(AuthProvider, { foreignKey: 'userId' });

// END: MYSQL RELATIONS

database
    .sync({force: config.seed})
    .then(function () {
        console.log("Database in Sync Now");
        require("./seed")();
    });

module.exports = {
    User: User,
    //Post: Post,
    //Comment: Comment,
    //AuthProvider: AuthProvider,
};
*/
/*
const SequelizeModels = require("sequelize-models");
 
var seqModels  = new SequelizeModels({
  // Database connection options 
  connection : {
    host     : "localhost",
    port     : 3306,
    logging  : console.log,
    dialect  : "mysql",
    username : "root",
    schema   : "pingu",
    password : "password1234"
  },
 
  // Models loading options 
  models : {
    autoLoad : true,
    path     : "/server/models"
  },
 
  // Sequelize options passed directly to Sequelize constructor 
  sequelizeOptions : {
    define : {
      freezeTableName : true,
      underscored     : true
    }
  }
});
*/
/*
seqModels.getSchema().then( schema => {
  // schema.models and schema.db available here

})
.catch( err => {
  // throwing error out of the promise 
  setTimeout( () => { throw err });
});*/

//var users = require('./models/users')(connection, Sequelize);
/*// create
app.post(PINGU_API, (req, res) => {
    var user = req.body;
    console.log(">>> " + JSON.stringify(req.body));
    users.create(user).then((result) => {
        console.log(result);
        res.status(200).json(result);
    }).catch((error) => {
        console.log(error);
        res.status(500).json(error);
    });
});
// update
app.put(PINGU_API, (req, res) => {
    console.log(">>> " + JSON.stringify(req.body));
    console.log("update user ..." + req.body);
    console.log(req.body.userid);
    var userid = req.body.userid;
    console.log(userid);
    var whereClause = { limit: 1, where: { userid: userid } };
    users.findOne(whereClause).then((result) => {
        result.update(req.body);
        res.status(200).json(result);
    }).catch((error) => {
        console.log(error);
        res.status(500).json(error);
    });
});
// retrieve 
app.get(PINGU_API, (req, res) => {
    console.log("search > " + req.query.keyword);
    var keyword = req.query.keyword;
    var sortby = req.query.sortby;
    var itemsPerPage = parseInt(req.query.itemsPerPage);
    var currentPage = parseInt(req.query.currentPage);
    var offset = (currentPage - 1) * itemsPerPage;
    console.log(offset);
    if (sortby == 'null') {
        console.log("sortby is null");
        sortby = "ASC";
    }
    console.log(keyword);
    console.log(itemsPerPage);
    console.log(currentPage);
    console.log(typeof offset);

    console.log("sortby " + sortby);
    console.log(typeof (keyword));
    if (keyword == '') {
        console.log("keyword is empty ?");
    }
    var whereClause = { offset: offset, limit: itemsPerPage, order: [['first_name', sortby], ['last_name', sortby]] };
    console.log(keyword.length);
    console.log(keyword !== 'undefined');
    console.log(keyword != undefined);
    console.log(!keyword);
    console.log(keyword.trim().length > 0);

    if ((keyword !== 'undefined' || !keyword) && keyword.trim().length > 0) {
        console.log("> " + keyword);
        whereClause = { offset: offset, limit: itemsPerPage, order: [['first_name', sortby], ['last_name', sortby]], where: { first_name: keyword } };
    }
    console.log(whereClause);
    users.findAndCountAll(whereClause).then((results) => {
        res.status(200).json(results);
    }).catch((error) => {
        console.log(error);
        res.status(500).json(error);
    });
});
app.get(PINGU_API + "/:userid", (req, res) => {
    console.log("one user ...");
    console.log(req.params.userid);
    var userid = req.params.userid;
    console.log(userid);
    var whereClause = { limit: 1, where: { userid: userid } };
    users.findOne(whereClause).then((result) => {
        res.status(200).json(result);
    }).catch((error) => {
        console.log(error);
        res.status(500).json(error);
    });
});
app.delete(PINGU_API + "/:userid", (req, res) => {
    console.log("one user ...");
    console.log(req.params.userid);
    var userid = req.params.userid;
    console.log(userid);
    var whereClause = { limit: 1, where: { userid: userid } };
    users.findOne(whereClause).then((result) => {
        result.destroy();
        res.status(200).json({});
    }).catch((error) => {
        console.log(error);
        res.status(500).json(error);
    });
});*/