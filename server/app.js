'use strict';
var express = require("express");
var path = require("path");
var bodyParser = require("body-parser");
var cookieParser = require('cookie-parser')
var Sequelize = require("sequelize");
var session = require('express-session');
var passport = require('passport');
// var config = require('./models/users');
const NODE_PORT = process.env.PORT || process.env.NODE_PORT || 4000;
// const db = require('./db');
// const PINGU_API = "/api/PAF";
const SQL_USERNAME = "root";
const SQL_PASSWORD = "password1234";
var connection = new Sequelize(
    'pingu',
    SQL_USERNAME,
    SQL_PASSWORD,
    {
        host: 'localhost',
        port: 3306,
        logging: console.log,
        dialect: 'mysql',
        dialectOptions: {
            useUTC: false //for reading from database
        },
        pool: {
            max: 10,
            min: 0,
            idle: 20000,
            acquire: 20000
        }
    }
);
connection
.authenticate()
.then(() => {
  console.log('Connection has been established successfully.');
})
.catch(err => {
  console.error('Unable to connect to the database:', err);
});
/*
const SequelizeModels = require("sequelize-models");

var seqModels  = new SequelizeModels({
 // Database connection options 
 connection : {
   host     : "localhost",
   port     : 3306,
   logging  : console.log,
   dialect  : "mysql",
   username : "root",
   schema   : "pingu",
   password : "password1234"
 },

 // Models loading options 
 models : {
   autoLoad : true,
   path     : "/models"
 },

 // Sequelize options passed directly to Sequelize constructor 
 sequelizeOptions : {
   define : {
     freezeTableName : true,
     underscored     : true
   }
 }
});
seqModels.getSchema().then( schema => {
  // schema.models and schema.db available here
    getSchema(pingu.users);
})
.catch( err => {
  // throwing error out of the promise 
  setTimeout( () => { throw err });
});*/
const CLIENT_FOLDER = path.join(__dirname, '/../client');
const MSG_FOLDER = path.join(CLIENT_FOLDER, '/assets/messages');
var app = express();
app.use(cookieParser());
app.use(express.static(CLIENT_FOLDER));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
var auth = require('./auth')(app, passport);
require('./routes')(auth, app, passport);
var SESSION_SECRET = "javpingu";
app.use(session({
    secret: SESSION_SECRET,
    resave: true,
    saveUninitialized: true,
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, '/../client')));
app.use(function (req, res) {
    res.status(404).sendFile(path.join(MSG_FOLDER + "/404.html"));
});
app.use(function (err, req, res, next) {
    res.status(500).sendFile(path.join(MSG_FOLDER + '/500.html'));
});
app.listen(NODE_PORT, function () {
    console.log("Server running at http://localhost:" + NODE_PORT);
});
module.exports = app;